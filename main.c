#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "generator.h"
#include "baza.h"
#include "help.h"
#include "stat.h"
int main(int argc, char** argv)
{

	int nGramy = 2;
	printf("Witamy w AGT - Automatycznym Generatorze Tekstu \n");
	if (argv[1] != NULL){
		if (strcmp(argv[1], "-help") == 0){ //tu zajmij się obsługą plików pomocy
			PrintFullHelp();
			return 0;
		}
		else if (strcmp(argv[1], "-stats") == 0){ //tu zajmij się obsługą statystyk
			if (argv[2] != NULL){
				if (strcmp(argv[2], "-all") == 0){
					printfAllStats();
				}
				else if (strcmp(argv[2],"-word")==0)
				{
					printfMostCommonWord("ExampleWord","PrzykladowyTeskt",1);
				}
				else if (strcmp(argv[2], "-n") == 0)
				{
					printfMostCommonNGram("ExamlpeNGram", "PrzykladowyTekst");
				}
				else if (strcmp(argv[2], "-part") == 0)
				{
					printfPartOfTextOccupied("slowo", "tekst"); //tu trzeba bedzie to i owo pobierac i sprawdzać. 
				}
				else{
					printf("Podałeś złą flagę.");
					printShortHelp();
					return 12;
				}
			}
			else{
				printf("Nie sprecyzowano jakie statystyki są pożądane.");
				PrintShortHelp();
				return 11;
			}
				
			return 0;
		}
		else if (strcmp(argv[1], "-new") == 0){
			if (argv[2] != NULL){
				if (strcmp(argv[2], "-n") == 0){
					printf("Tu czytam ilo gramowy ma być generator");
					nGramy = argv[3];
					if (argv[4] != NULL & strcmp(argv[4], "-limits") == 0){
						if (argv[5] != NULL & argv[6] != NULL){
							if (strcmp(argv[7], "-text") == 0){
								int i;
								for (i = 8; i<argc; i++)
								{
									printf("Załadowano plik: %s\n", argv[i]);
								}
								printf("Generuje tekst z załadowanych plików, będzie on miał %s slow i %s wierszy z ngramami\n", argv[5], argv[6]);
							}

						}
						else{
							PrintShortHelp();
							printf("Nie podałes ilosci wierzy lub slow\n");
							return 6;
						}

					}
					else if (argv[4] != NULL & strcmp(argv[4], "-text") == 0){
						int i;
						for (i = 5; i<argc; i++)
						{
							printf("Załadowano plik: %s\n", argv[i]);
						}
						printf("Generuje tekst z załadowanych plików z ngramami");
						//      Generate();
					}

				}

				else if (strcmp(argv[2], "-limits") == 0){

					if (argv[3] != NULL & argv[4] != NULL){
						if (strcmp(argv[5], "-text") == 0)
						{
							int i;
							for (i = 6; i<argc; i++)
								printf("Załadowano plik: %s\n", argv[i]);
							printf("Generuje tekst z załadowanych plików, będzie on miał %s slow i %s wierszy z ngramami\n", argv[3], argv[4]);
						}


					}
					else{
						printf("Nie podałes ilosci wierzy lub slow\n");
						return 6;
					}
				}
				else if (strcmp(argv[2], "-text") == 0){
					int i;
					for (i = 3; i<argc; i++)
					{
						printf("Załadowano plik: %s\n", argv[i]);
					}
					printf("Generuje tekst z załadowanych plików\n");
				}
				else{
					printf("Brak niezbędnej flagi\n");
					return 3;
				}
				return 0;
			}
			else
			{
				printf("Brak niezbędnej flagi\n");
				return 10;
			}
		}
		else if (strcmp(argv[1], "-used") == 0){ // jest ok
			if (argv[2] != NULL & strcmp(argv[2], "-name") == 0)
			{
				if (argv[3] != NULL){
					printf("Tu następuje generacja na podstawie bazy %s\n", argv[3]);
					generateFromBase();
					return 0;
				}
				else{
					printf("Brak podanej nazwy aktualnie używanej bazy\n");
					return 4;
				}
			}
			return 0;
		}
		else if (strcmp(argv[1], "-settings") == 0){
			if (argv[2] != NULL & strcmp(argv[2], "-name") == 0)
			{
				if (argv[3] != NULL){
					printf("Tu następuje wyswietlenie bazy o numerze", argv[3]);
					generateFromBase();
					return 0;
				}
				else{
					printf("Brak podanej nazwy aktualnie używanej bazy\n");
					return 4;
				}
			}
			else if (argv[2] != NULL & strcmp(argv[2], "-all") == 0)
			{
				printf("Wypisuje wszystko/n");
				return 7;
			}
			else{
				printf("Błąd we fladze\n");
				return 99;
			}
			return 0;
		}
		else{
			printf("Bład flagi \n");
			PrintShortHelp();
			return 1;
		}
	}
	else{
		printf("Brak niezbędnej flagi \n");
		PrintShortHelp();
		return 2;
	}

	return 0;
}

