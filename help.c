#include <stdio.h>
#include "help.h"

void PrintShortHelp(){
  printf("Program AGT służy do automatycznej generacji tekstów. \n Tekst generować możesz na podstawie istniejącej już bazy lub stowrzyć nową. \n Poza tym program może wyświetlić 
	  statystyki dotyczące generowanych tekstów lub listę dostępnych baz.\n Aby nauczyć się wywoływać konkretne funkcje programu i poprawnie z niego korzystać uruchom program z 
	  flagą -help.");
}

void PrintFullHelp(){
  printf("Dostępne tryby uruchomienia: \n 1.flaga -new tworzy nową bazę i generuje teskt na jej podstawie. \n Aby skonfigurować bazę aplikacja udostępnia flagi \n 
	  -n a (a to stopień n-gramów na podstawie których generowany będzie tekst )\n
	  -limits a b (liczby całkowite a i b oznaczają ilość słów i wierszy w generowanym tekście, domyślnie wartości losowe)\n
	  -text (po nim podajesz dowolną ilość adresów tekstów bazowych) \n
	  Jedyną obowiązkową flagą jest -text, aczkolwiek jeśli podajemy ich więcej, to muszą być podane w takiej kolejności (-n,-limits,-text)\n");
}
