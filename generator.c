#include "generator.h"
#include "baza.h"

const int INITX = 100;
const int INITY = 100;

void InitGenerator(GeneratorT* gene, int slowa, int wiersze, int teksty, int rzad)
{
	gene.iloscSlow = slowa;
	gene.iloscWierszy = wiersze;
	gene.iloscTekstow = teksty;
	gene.rzadNGramu = rzad;
	gene.ileTekstowZaladowano = 0;
}

void Generate(int rzadNGramu, int iloscSlow, int iloscWierszy, int ileTekstowBazowych, char** tekstyBazowe, bool czyZapisacBaze, bool czyZapisacWynik)
{
	BaseT baza;
	InitBaza(&baza, iloscSlow, iloscWierszy, ileTekstowBazowych, rzadNGramu);
	int i;
	for (i = 0; i < baza.ileTekstow; i++)
	{
		AddContent(&baza, tekstyBazowe[i]);
	}
	GeneratorT generator;
	InitGenerator(&generator, &baza);
	PrzygotujBaze(&generator, &baza);
	Generuj(&generator);

}

void DodajTekstBazowy(struct Generator* gene, char* tekst)
{
	int staryRozmiar = sizeof(gene.tekstBazowy);
	int rozmiarNowegoTekstu = sizeof(tekst);
	gene->tekstBazowy = (char*)realloc(gene, staryRozmiar + rozmiarNowegoTekstu + 1);
	gene->tekstBazowy = strcat(gene->tekstBazowy, tekst);

}

void WypiszUstawienia(struct Generator* gene)
{
	printf("Generowany tekst bedzie mial: %d slow. ", gene->iloscSlow);
	printf("Generowany tekst bedzie mial: %d wierszy. ", gene->iloscWierszy);
	printf("Generowany tekst bedzie generowany z: %d-gramow. ", gene->rzadNGramu);
	printf("Generowany tekst będzie na bazie %d tekstów", gene->iloscTekstow);
}

void Generuj(struct Generator* gene) //zastanów się nad generowaniem innych głupot jak np. ile ma być wierszów, albo interpunkcja
{
	int i,j;
	int wordNumber, howManyOcurrence, sufixNumber;
	howManyOcurrence = 0;
	int howManyPrefixes=gene->XWords.size;
	char* prefix = malloc(1024*sizeof(char));
	char* sufix = malloc(1024 * sizeof(char));
	char* generatedText = malloc(1024 * (gene->usedBase.iloscSlow + 1)*sizeof(char)); //cały tekst będzie miał tyle pamieci
	int seed = time(NULL);
	srand(seed);
	wordNumber = rand() % howManyPrefixes;
	prefix = gene->XWords.content[wordNumber];
	for (j = 0; j < gene->usedBase.iloscSlow; j++)
	{
		sprintf(generatedText, "%s ", prefix); //zapisz poprzedni prefix
		for (i = 0; i < gene->YWords.size; i++) //ile razy prefix wystąpił w tekscie
		{
			howManyOcurrence += gene->occurrenceOfWords.content[wordNumber][i];
		}
		sufixNumber = rand() % howManyOcurrence; // losuj numer z przedziału <0,liczba wystapien>
		for (i = 0; 0 <= suffixNumber; i++) // sprawdz jaki sufix wylosowałeś
		{
			suffixNumber -= gene->occurrenceOfWords.content[wordNumber][i];
		}
		prefix = gene->YWords.content[i]; //sufix zostaje prefixem, by zaraz dać się zapisać
	}
	sprintf(generatedText, "%s.", prefix); //zapisz ostatni sufkis
	printf(generatedText);
	saveBase();
	saveText();
}

void PrzygotujBaze(GeneratorT* gene, BazaT* baza)
{
	//napoczątek używam wersji tylko z bigramami, potem postaram się to rozbudowac
	int rzad = 2;
	char* prefix = malloc(1024 * sizeof(char);
	char* sufix = malloc((rzad - 1) * 1024 * sizeof(char));
	int i = 0;
	while (baza->ileTekstowZaladowano < baza->ileTekstow)
	{
		//char* tmp = malloc(sizeof(baza->tekstyBazowe[i]));
		char* tmp = &(baza->tekstyBazowe[i]);
		if (sscanf(tmp, "%s", prefix) == 1)
		{
			while (sscanf(tmp, "%s" sufix) == 1)
			{
				int PPosition = searchForWord(gene->XWords, prefix, gene->occurrenceOfWords);
				int SPosition = searchForWord(gene->YWords, sufix, gene->occurrenceOfWords);
				gene->occurrenceOfWords->content[PPosition, SPosition]++;
				prefix = sufix;
			}
			i++;
		}
	}
}

int searchForWord(wordArray* tablica, char* word, DArray* tablica2D) //szukasz prefiksu w tablicy, jeśli znajdziesz zwracasz jego pozycje, jeśli nie, to dodajesz do listy i wtedy zwracasz pozycje
{
	int i;
	for (i = 0; i < tablica->sizeX; i++)
	{
		if (strcmp(tablica->content, word) == 0)
			return i;
		addwordArray(i, word, tablica, tablica2D); //zadbaj o zgodności na tablicy, w tej funkcji i o dodanie 0
		return i;
	}
}



void initwordArray(wordArray* tablica){
	/*int i;
	tablica = malloc(sizeof(wordArray));
	tablica->content = (char**)malloc(100 * sizeof(char*));
	for (i = 0; i < 100; i++)
	{
	tablica->content[i] = malloc(sizeof(char) * 1024);
	}*/
	tablica = malloc(sizeof(stringT) * INITX);
	tablica->sizeX = INITX;
}
void addwordArray(int x, char* value, wordArray* tablica, DArray * tablica2D, bool isX){
	if (tablica->sizeX > x) //dodajesz słowo bez przypału
	{
		tablica->content[x].word = value;
		int i;
		if (isX == bool.true){
			for (i = 0; i < tablica->sizeY; i++)
			{
				tablica2D->content[x][i] = 0;
			}
		}
		else
		{
			for (i = 0; i < tablica->sizeX; i++)
			{
				tablica2D->content[i][x] = 0;
			}
		}
	}
	else
	{
		void *tmp;
		if ((tmp = realloc(tablica->content, 2 * sizeof(tablica->content))) != NULL)
		{
			tablica->content = tmp;
			tablica->sizeX *= 2;
			tablica->content[x] = value;
			realloc2dArray(tablica2D);
			tablica2D->content[x][y]++;
		}
		else
		{
			fprintf("Problem z dodawaniem słowa. Kod błędu 001.");
		}

	}


}
void freewordArray(wordArray* tablica){
	free(tablica;)
}

void init2dArray(DArray * tablica)
{
	int i, j;
	tablica = malloc(sizeof(DArray));
	tablica->content = (int**)malloc(INITX * sizeof(int*));
	for (i = 0; i < INITX; i++)
	{
		tablica->content[i] = malloc(INITY * sizeof(int));
		for (j = 0; j < INITY; j++)
		{
			tablica->content[i][j] = 0;
		}
	}
	/*tablica = malloc(INITX * INITY *sizeof(int));*/
	tablica->sizeX = INITX;
	tablica->sizeY = INITY;
}
//void add2dArray(int x, int y, int value, 2dArray * tablica){
//      int i, j;
//      if (2dArray->sizeX > x && 2dArray->sizeY > y)
//      {
//
//      }
//}
void free2dArray(DArray * tablica){
	free(tablica);
}


void realloc2dArray(DArray* tablica)
{
	void* tmp;
	if ((tmp = realloc(tablica2D->content, 4 * sizeof(tablica2D->content))) != NULL)
	{
		int i, j;
		tablica2D->content = tmp;
		for (i = 0; i < (2 * tablica2D->sizeX); i++)
		{
			void* tmp;
			if ((tmp = realloc(tablica2D->content[i], 2 * sizeof(tablica2D->content[i]))) != NULL)
			{
				tablica2D->content[i] = tmp;
				for (j = 0; j < (2 * tablica2D->sizeY); j++)
				{
					if (i>tablica2D->sizeX && j>tablica2D->sizeY)
						tablica->content[i][j] = 0;
				}
			}
		}
	}
	else
	{
		fpintf("Problem z dodawaniem słowa. Kod błędu 002.");
	}
}
void saveBase()
{

}
void saveText()
{

}