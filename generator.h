#include <stdlib.h>
#include <time.h>
#include "baza.h"
#include "ls.h"

typedef struct {
	int sizeX;
	int sizeY;
	int** content;

} DArray;

void initDArray(DArray* tablica);
void addDArray(int x, int y, int value, DArray* tablica);
void freeDArray(DArray* tablica);

typedef struct {
	char word[1024];
} stringT;


typedef struct {
	int size;
	stringT* content;
} wordArray;

void initwordArray(wordArray* tablica);
void addwordArray(int x, char* value, wordArray* tablica);
void freewordArray(wordArray* tablica);

typedef struct{
	wordArray XWords; //tablica prefixów
	wordArray YWords; // tablica sufixów
	DArray occurrenceOfWords; // tablica dwuwymiarowa pokazująca ile miejsc
	//double** frequencyOfWords;
	BaseT usedBase;
} GeneratorT;

void InitGenerator(GeneratorT*, BaseT*);

void DodajTekstBazowy(GeneratorT*, char*);

void WypiszUstawienia(GeneratorT*);

void PrzygotujBaze(GeneratorT*, BaseT*);

void Generuj(GeneratorT*);

void Generate(int rzadNGramu, int iloscSlow, int iloscWierszy, int ileTekstow, char** tekstyBazowe, bool czyZapisacBaze, bool czyZapisacWynik);

void realloc2dArray(DArray* tablica);

void saveBase();

void saveText();
