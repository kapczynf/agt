#include <stdio.h>
#include <stdlib.h>
#include "generator.h"
#include "ls.h"

//typedef enum { false, true } bool;

//tu r�wnie� pami�taj, �eby u�yc makrodyrektywy do zapepiecznia bool-type i b�dziesz m�g� upro�ci� komunikacj�
//Mo�esz spr�bowac komunikacj� odwzorowa� identycznie jak w specyfikacji, na diagrami

//pami�taj o tym, �e w tym module b�dzie znajdowa�o si� obs�ugiwanie -settings

typedef struct {
	int rzadNGramu;
	int iloscSlow;
	int iloscWierszy;
	int ileTekstow;
	int ileTekstowZaladowano;
	char** tekstyBazowe;
} BaseT;

void InitBase(BaseT* baza, int slowa, int wiersze, int teksty, int rzad);
void AddContent(BaseT* baza, char* content);
void PrintBases();

